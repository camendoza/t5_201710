package t5_201710;

import junit.framework.TestCase;
import model.data_structures.ColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.exceptions.TamanioMaximoException;
import model.logic.ManejadorPreferencias;
import model.vo.VOPreferencia;

public class PruebaPreferencias extends TestCase
{
	private ManejadorPreferencias manejador;
	private VOPreferencia mayor = new VOPreferencia();
	/**
	 * Constante para cambiar el g�nero de la muestra. En este caso Drama.
	 */
	private final static String genero = "Drama";
	public void setupEscenario1()
	{
		manejador = new ManejadorPreferencias();
		manejador.cargarArchivo("data/movies.csv");
		System.out.println("G�nero utilizado: " + genero);
	}
	public void testCrearColaP() throws TamanioMaximoException
	{
		setupEscenario1();
		mayor.setAgno(2016);
		mayor.setPelicula("Mohenjo Daro");
		long start = System.currentTimeMillis();
		ColaPrioridad<VOPreferencia> preferencias = manejador.crearColaP(genero);
		long end = System.currentTimeMillis();
		System.out.println("   Tiempo para crear colaPrioridad (ms):" + (end-start));
		assertEquals(4365, preferencias.darNumElementos());
		start = System.currentTimeMillis();
		VOPreferencia aux = preferencias.max();
		end = System.currentTimeMillis();
		System.out.println("   Tiempo para sacar el m�ximo de colaPrioridad (ms):" + (end-start));
		assertEquals(mayor.getAgno(), aux.getAgno());
		assertTrue(aux.getPelicula().trim().equals("Mohenjo Daro"));
	}
	public void testCrearMonticuloCP() throws TamanioMaximoException
	{
		setupEscenario1();
		mayor.setAgno(2016);
		mayor.setPelicula("Mohenjo Daro");
		long start = System.currentTimeMillis();
		MaxHeapCP<VOPreferencia> preferencias = manejador.crearMonticuloCP(genero);
		long end = System.currentTimeMillis();
		System.out.println("   Tiempo para crear monticulo (ms):" + (end-start));
		assertEquals(4365, preferencias.darNumeroElementos());
		start = System.currentTimeMillis();
		VOPreferencia aux = preferencias.max();
		end = System.currentTimeMillis();
		System.out.println("   Tiempo para sacar el m�ximo de monticulo (ms):" + (end-start));
		assertEquals(mayor.getAgno(), aux.getAgno());
		assertTrue(aux.getPelicula().trim().equals("Mohenjo Daro"));
	}
}
