package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> first;
	private NodoDoble<T> last;
	
	public ListaDobleEncadenada( )
	{
		first = null;
		last = null;
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> nElem = new NodoDoble<T>( elem );
		if( first == null )
		{
			first = nElem;
			last = nElem;
		}else
		{
			nElem.setPrevious(last);
			last.setNext(nElem);
			last = nElem;
			last = last.getNext();
		}
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		return false;
		
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

}
