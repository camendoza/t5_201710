package model.data_structures;

import model.exceptions.TamanioMaximoException;

public class MaxHeapCP <T extends Comparable<T>>
{
	private T[] heap;
	private int size = 0;
	public void crearCP( int max )
	{
		heap = ( T[] )new Comparable[max+1];
	}
	public int darNumeroElementos()
	{
		return size;
	}
	public void agregar(T elemento) throws TamanioMaximoException
	{
		try
		{
			heap[++size] = elemento;
			swim(size);
		}catch(Exception e)
		{
			size--;
			throw new TamanioMaximoException(e.getMessage() + " - No se pudo agregar el elemento. Tama�o m�ximo excedido");
		}
	}
	public T max()
	{
		if(esVacia())
			return null;
		T max = heap[1];
		exch(1,size--);
		sink(1);
		heap[size+1] = null;
		return max;
	}
	
	public boolean esVacia()
	{
		return size==0;
	}
	public int tamanoMax()
	{
		return heap.length;
	}
	// --------------------------------------------------------------------
	// M�todos de ayuda
	// --------------------------------------------------------------------
	private void swim(int i) {
		while(i>1 && less(i/2, i))
		{
			exch(i, i/2);
			i = i/2;
		}
	}
	private void exch(int i, int j) {
		T temp = heap[i];
		heap[i]=heap[j];
		heap[j]=temp;
	}
	private boolean less(int i, int j) {
		boolean b = heap[i].compareTo(heap[j])<0;
		return b;
	}
	private void sink(int i) {
		while(2*i <= size)
		{
			int j = 2*i;
			if (j < size && less(j, j+1))
				j++;
			if(!less(i, j))
				break;
			exch(i, j);
			i = j;
		}
	}
}
