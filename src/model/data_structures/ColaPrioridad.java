package model.data_structures;

import model.exceptions.TamanioMaximoException;

public class ColaPrioridad <T extends Comparable<T>> {
	
	private ILista<T> lista;
	private int size;
	private int max;
	public void crearCP(int nMax)
	{
		size =0;
		lista = new ListaEncadenada<T>();
		max= nMax;
	}
	
	public int darNumElementos()
	{
		return size;
	}
	
	public void agregar(T elemento) throws TamanioMaximoException
	{
		if(size==max)
		{
			throw new TamanioMaximoException("");
		}
		else
		{
			lista.agregarElementoFinal(elemento);
			size++;
		}
	}
	
	public T max()
	{
		if(size==0)
		{
			return null;
		}
		else if(lista!=null)
			
		{   T max = lista.darElemento(0);
		    int ind =0;
		    
			for(int i=0; i<lista.darNumeroElementos();i++)
			{ 
			     T act = lista.darElemento(i);
				if(act!=null)
				if(max.compareTo(act)<0)
				{
					max = act;
				}
				ind++;
			}
			lista.eliminarElemento(ind);
			size--;
			return max;
			
		}
		return null;
		
	}
	public int tamanoMax()
	{
		return max;
	}
	
	public boolean esVacia()
	{
		return size==0;
	}
	
	

}
