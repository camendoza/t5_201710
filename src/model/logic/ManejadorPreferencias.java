package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;

import model.data_structures.ColaPrioridad;
import model.data_structures.ListaEncadenada;
import model.data_structures.MaxHeapCP;
import model.exceptions.TamanioMaximoException;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOPreferencia;

public class ManejadorPreferencias
{
	private ListaEncadenada<VOPelicula> misPeliculas;

	private ListaEncadenada<VOGeneroPelicula> listaGeneros;

	public void cargarArchivo(String rutaPeliculas) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(rutaPeliculas));
			String linea = br.readLine();
			misPeliculas = new ListaEncadenada<>();
			linea = br.readLine();
			int ind = 1;

			try {
				while (linea != null && ind < 9125) {
					linea = br.readLine();
					if (linea.indexOf("\"") == -1) {
						String casillas[];
						casillas = linea.split(",");
						long id = Long.parseLong(casillas[0]); 
						String titleAndYear = casillas[1];
						String prueba[] = titleAndYear.split("\\(");
						String title;
						String year;
						if (prueba.length == 1) {
							title = titleAndYear;
							year = "0";
						} else if (prueba.length == 2) {
							title = titleAndYear.split("\\(")[0];
							year = titleAndYear.split("\\(")[1];
						} else if (prueba.length == 3) {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1];
							year = titleAndYear.split("\\(")[2];
						} else if (prueba.length == 4) {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1] + "("
									+ titleAndYear.split("\\(")[2];
							year = titleAndYear.split("\\(")[3];
						} else {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1] + "("
									+ titleAndYear.split("\\(")[2] + "(" + titleAndYear.split("\\(")[3];
							year = titleAndYear.split("\\(")[4];
						}
						String genres = casillas[2];
						String[] listaGeneros = genres.split("\\|");

						VOPelicula pelicula = new VOPelicula();

						ListaEncadenada<String> listGenres = new ListaEncadenada<String>();
						for (int i = 0; i < listaGeneros.length; i++) {
							listGenres.agregarElementoFinal(listaGeneros[i]);
						}
						pelicula.setTitulo(title);
						pelicula.setAgnoPublicacion(Integer.parseInt(year.split("\\)")[0].split("-")[0]));
						pelicula.setGenerosAsociados(listGenres);
						misPeliculas.agregarElementoFinal(pelicula);
						ind++;
					} else {
						String lineas[] = linea.split("\"");
						long id = Long.parseLong(lineas[0].split(",")[0]);
						String titleAndYear;
						String genres;
						if (lineas.length == 3) {
							titleAndYear = lineas[1];
							genres = lineas[2].split(",")[1];
						} else if (lineas.length == 5) {
							titleAndYear = lineas[1] + "\"" + lineas[2] + lineas[3];
							genres = lineas[4].split(",")[1];
						} else {
							titleAndYear = lineas[1] + lineas[2] + "\"" + lineas[3] + "\"" + lineas[4] + lineas[5];
							genres = lineas[6].split(",")[1];
						}
						String prueba[] = titleAndYear.split("\\(");
						String title;
						String year;
						if (prueba.length == 1) {
							title = titleAndYear;
							year = "0";
						} else if (prueba.length == 2) {
							title = titleAndYear.split("\\(")[0];
							year = titleAndYear.split("\\(")[1].split("\\)")[0];
						} else if (prueba.length == 3) {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1];
							year = titleAndYear.split("\\(")[2].split("\\)")[0];
						} else if (prueba.length == 4) {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1] + "("
									+ titleAndYear.split("\\(")[2];
							year = titleAndYear.split("\\(")[3].split("\\)")[0];
						} else {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1] + "("
									+ titleAndYear.split("\\(")[2] + "(" + titleAndYear.split("\\(")[3];
							year = titleAndYear.split("\\(")[4].split("\\)")[0];
						}
						String[] listaGeneros = genres.split("\\|");

						VOPelicula pelicula = new VOPelicula();

						ListaEncadenada<String> listGenres = new ListaEncadenada<String>();
						for (int i = 0; i < listaGeneros.length; i++) {
							listGenres.agregarElementoFinal(listaGeneros[i]);
						}
						pelicula.setTitulo(title);
						pelicula.setAgnoPublicacion(Integer.parseInt(year.split("-")[0]));
						pelicula.setGenerosAsociados(listGenres);
						misPeliculas.agregarElementoFinal(pelicula);
						ind++;
					}
				}
			} catch (Exception e) {
				System.out.println("Error en pel�cula: " + linea.split(",")[0]);
				System.out.println(linea);
				System.out.println(e.getMessage());
				System.out.println("ind: " + ind);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage() + "Fallo en el inicio");
		}
		crearListaGeneros();
	}

	private void crearListaGeneros() {
		listaGeneros = new ListaEncadenada<VOGeneroPelicula>();
		for (VOPelicula peliculaActual : misPeliculas) {
			ListaEncadenada<String> generosActual = (ListaEncadenada<String>) peliculaActual.getGenerosAsociados();

			for (String generoActualPeliculas : generosActual) {


				if (listaGeneros.darNumeroElementos()==0) {
					VOGeneroPelicula nuevoGenero = new VOGeneroPelicula();
					ListaEncadenada<VOPelicula> nuevaListaPeliculas = new ListaEncadenada<>();
					nuevaListaPeliculas.agregarElementoFinal(peliculaActual);
					nuevoGenero.setGenero(generoActualPeliculas);
					nuevoGenero.setPeliculas(nuevaListaPeliculas);
					listaGeneros.agregarElementoFinal(nuevoGenero);

				}
				else
				{
					int ind =0;
					boolean encontro = false;
					for (VOGeneroPelicula generoDeListaGeneros : listaGeneros) 
					{
						if (generoActualPeliculas.equals(generoDeListaGeneros.getGenero())) {
							generoDeListaGeneros.getPeliculas().agregarElementoFinal(peliculaActual);
							encontro = true;
						}
						ind++;
					} 
					if(encontro==false&&ind==listaGeneros.darNumeroElementos())
					{   VOGeneroPelicula nuevoGenero = new VOGeneroPelicula();
					ListaEncadenada<VOPelicula> nuevaListaPeliculas = new ListaEncadenada<>();
					nuevaListaPeliculas.agregarElementoFinal(peliculaActual);
					nuevoGenero.setGenero(generoActualPeliculas);
					nuevoGenero.setPeliculas(nuevaListaPeliculas);
					listaGeneros.agregarElementoFinal(nuevoGenero);
					ind++;
					}

				}
			}
		}
	}

	public ColaPrioridad<VOPreferencia> crearColaP(String genero) throws TamanioMaximoException
	{
		ColaPrioridad<VOPreferencia> toReturn = new ColaPrioridad<>();
		VOPreferencia toAdd = new VOPreferencia();
		for(VOGeneroPelicula actGeneroPelicula : listaGeneros)
		{
			if(actGeneroPelicula.getGenero().equalsIgnoreCase(genero))
			{
				toReturn.crearCP(actGeneroPelicula.getPeliculas().darNumeroElementos());
				for(VOPelicula actPelicula : actGeneroPelicula.getPeliculas())
				{
					toAdd.setPelicula(actPelicula.getTitulo());
					toAdd.setAgno(actPelicula.getAgnoPublicacion());
					toReturn.agregar(toAdd);
				}
			}
		}
		return toReturn;
	}
	public MaxHeapCP<VOPreferencia> crearMonticuloCP(String genero) throws TamanioMaximoException
	{
		MaxHeapCP<VOPreferencia> toReturn = new MaxHeapCP<>();
		VOPreferencia toAdd = new VOPreferencia();
		for(VOGeneroPelicula actGeneroPelicula : listaGeneros)
		{
			if(actGeneroPelicula.getGenero().equalsIgnoreCase(genero))
			{
				toReturn.crearCP(actGeneroPelicula.getPeliculas().darNumeroElementos());
				for(VOPelicula actPelicula : actGeneroPelicula.getPeliculas())
				{
					toAdd.setPelicula(actPelicula.getTitulo());
					toAdd.setAgno(actPelicula.getAgnoPublicacion());
					toReturn.agregar(toAdd);
				}
			}
		}
		return toReturn;
	}
}
