package model.vo;

import model.data_structures.ILista;

public class VOPelicula implements Comparable<VOPelicula>
{
	private String titulo;
	private int agnoPublicacion;
	private ILista<String> generosAsociados;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	@Override
	public int compareTo(VOPelicula arg0) {
		if (titulo.compareTo(arg0.getTitulo())>0)
			return 1;
		if (titulo.compareTo(arg0.getTitulo())<0)
			return -1;
		return 0;
	}
}
