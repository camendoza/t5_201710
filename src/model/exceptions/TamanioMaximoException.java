package model.exceptions;

public class TamanioMaximoException extends Exception
{

	/**
	 * Constatnte para la serialización.
	 */
	private static final long serialVersionUID = 112233445566L;
	
	
	public TamanioMaximoException( String mensaje)
	{
		super(mensaje);
	}
	
}
